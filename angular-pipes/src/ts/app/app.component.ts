import { Component } from "@angular/core";
import { Person } from "./person";

@Component({
    selector: "main",
    template: require("./app.component.html"),
})
export class AppComponent {

    public currentPage: number = 0;
    public pageLength: number = 3;

    public people: Person[] = [
        new Person("Farooq", "Mahmud"),
        new Person("Emily", "Green"),
        new Person("Noor", "Mahmud"),
        new Person("Yasin", "Mahmud"),
        new Person("Andrew", "Green"),
        new Person("Chris", "Kelly"),
        new Person("Andrew", "Plata"),
        new Person("Mark", "Smith"),
        new Person("Aaron", "Schraufnagel"),
        new Person("Matt", "Netkow"),
    ];

    public get startPerson(): number {
        return this.currentPage * this.pageLength;
    }

    public get endPerson(): number {
        return (this.currentPage + 1) * this.pageLength;
    }

    public previousPage() {
        if (this.currentPage > 0) {
            this.currentPage--;
        }
    }

    public nextPage() {

        let pages = this.people.length / this.pageLength;

        if (this.people.length / this.pageLength > 0) {
            pages++;
        }

        if (this.currentPage < pages) {
            this.currentPage++;
        }
    }
}

