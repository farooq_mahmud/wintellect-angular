import { Component } from "@angular/core";

@Component({
    selector: "main",
    template: require("./app.component.html"),
    styles: [require("./app.component.scss")],
})
export class AppComponent {

    public message: string = "Hello Colors!";
    public colors: string[] = ["red", "orange", "green", "blue", "yellow", "gray"];
    public newColor: string = "";

    public addColor() {
        this.colors.push(this.newColor);
        this.newColor = "";
    }
}
